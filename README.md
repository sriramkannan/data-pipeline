# Installation
Clone this project
```
git clone <this project from gitlab>
```

# Setup
### Pre-requisites
Copy paste the following export commands with appropriate details filled in: 
```
export AWS_SUBNET_ID=<Private-Subnet-ID>
export AWS_VPC_ID=<VPC-ID>
export AWS_DEFAULT_REGION=us-east-1
export AWS_ZONE=a
export AWS_ACCESS_KEY_ID=xxx
export AWS_SECRET_ACCESS_KEY=xxx
```

### Create docker machines
Create a few docker machines by running thhe following shell script & follow the prompts. 
```
./createDockerMachine.sh
```

### Create the swarm

In the terminal, eval to the first created machine
```
eval $(docker-machine env elk-ls-01)
```

Enter the following command to create/ "initiate" a swarm
```
docker swarm init
```
Copy the "docker swarm join --token ......" command and use below

"eval" to the other docker machines one by one and enter the following command to join the swarm. Example below. 

```
eval $(docker-machine env elk-ls-02)
docker swarm join --token SWMTKN-1-5bub9ydodrmhxtyqn4slswzl9qj6i5iimrtlj0j42wtvh2c6nr-21tg4ahkhem3s6yu1x6xe1q51 10.64.1.230:2377
```


### below to be cleaned up.....

docker-machine create --driver amazonec2 --amazonec2-access-key xxx --amazonec2-secret-key xxx --amazonec2-region us-east-1 --amazonec2-zone a --amazonec2-subnet-id subnet-099560f34bbd7fd79 --amazonec2-vpc-id vpc-0759c99df19190cdd --amazonec2-instance-type t3.medium elk-ls-01

docker-machine create --driver amazonec2 --amazonec2-access-key xxx --amazonec2-secret-key xxx --amazonec2-region us-east-1 --amazonec2-zone b --amazonec2-subnet-id subnet-0221f42f3fc223266 --amazonec2-vpc-id vpc-0759c99df19190cdd --amazonec2-instance-type t3.medium elk-ls-02

docker-machine create --driver amazonec2 --amazonec2-access-key xxx --amazonec2-secret-key xxx --amazonec2-region us-east-1 --amazonec2-zone c --amazonec2-subnet-id subnet-0996e7d0bfe7bd23e --amazonec2-vpc-id vpc-0759c99df19190cdd --amazonec2-instance-type t3.medium elk-ls-03





To create a remote docker machine private Elaticsearch
docker-machine create --driver amazonec2 --amazonec2-access-key xxx --amazonec2-secret-key xxx --amazonec2-region us-east-1 --amazonec2-subnet-id subnet-0edfed0daa1289529 --amazonec2-vpc-id vpc-0759c99df19190cdd --amazonec2-private-address-only --amazonec2-instance-type t3.xlarge elk-es-01

Increase the storage volume size to 100 GB (for now)

docker-compose -f docker-compose-ek-stack.yml up -d elasticsearch
docker-compose -f docker-compose-ek-stack.yml up -d kibana


docker-machine scp ./logstash/config/logstash.yml elk-ls-01:/tmp/
docker-machine scp ./logstash/pipeline/logstash.conf elk-ls-01:/tmp/
docker-machine scp ./logstash/config/logstash.yml elk-ls-02:/tmp/
docker-machine scp ./logstash/pipeline/logstash.conf elk-ls-02:/tmp/
docker-machine scp ./logstash/config/logstash.yml elk-ls-03:/tmp/
docker-machine scp ./logstash/pipeline/logstash.conf elk-ls-03:/tmp/

cd logstash

docker stack deploy -c docker-compose.yml ls



beats

docker run -v /home/ubuntu/filebeat.yml:/usr/share/filebeat/filebeat.yml -v /tmp/:/tmp/ docker.elastic.co/beats/filebeat:6.4.0